# GoogleResultFile / GoogleResultFiles

Python package that reads Google search result files saved as HTML.

Import:

```python
from GoogleResultFile import *
```

## Individual search files

You can run it on individual search files:

```python
r = GoogleResultFile(file='./path-to/directory/with/search-files/1.html')
```

The resulting object contains some resources that are helpful

### All elements of the search results

You can see lists of the headers, abstracts, and links for each of the search results in the file easily.

```python
print(r.headers)
print(r.abstracts)
print(r.links)
```

You can also receive them as a list of tuples:

```python
print(r.results)
```

### Seeing potential text

The package also comes with a built-in method for downloading potential text from search results (via a URL call):

```python
r.get_potential_text()
r.get_potential_text(index=10)
```

You can pass an index (default is `0`), which tells you which search result in the file, you'd like to have returned.

If you try to pass an index that is outside of the amount of search results in the file, you will be thrown an `IndexError`.

### Top domains

You can also easily access a list of all the domains from each of the search results by accessing the property `domains`:

```python
r.domains
```

### Access file path and raw data

If, for some reason, you need to refer back to the filepath of the search result file, you can call:

```python
r.file
```

If you need to access to the BeautifulSoup interpretation of the raw HTML, you can get access to it:

```python
r.soup
```

## Multiple search files or directories of search files

You can also run the script on a directory or a set of files (mixed works as well):

```python
r = GoogleResultFiles(paths=['./path-to/directory-with-search-files/', './path-to-another-file.html])
```

### See a list of files

```python
r.files
```

### Loop through the files

If you want to loop through all of the search files as individual `GoogleResultFile` objects, a list of them is easily accessible in the `result_files` property:

```python
for file in r.result_files:
  # do something here
```

### All elements of the search results

Just as above, with the individual file, this collection compounds lists of the headers, abstracts, and links for the search results across all of the files easily.

```python
print(r.headers)
print(r.abstracts)
print(r.links)
```

You can also receive the search results as a list of tuples:

```python
print(r.results)
```

### Top domains

You can also easily access a list of all the domains from across all of the search results by accessing the `domains` property:

```python
r.domains
```

## Future features

I would like to make sure that each `GoogleResultFile` object is iterable, which would mean that each of the individual results (`results`) gets generated in each iteration.

Perhaps `GoogleResultFiles` could be iterable as well, iterating over the individual `GoogleResultFile` objects in the `result_files` property.
