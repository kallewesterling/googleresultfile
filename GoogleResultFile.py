import requests
from pathlib import Path
from bs4 import BeautifulSoup
from urllib.parse import urlparse


class GoogleResultFile():
    
    def __init__(self, file=None):
        self.file = Path(file)
        html = self.file.read_text()
        self.soup = BeautifulSoup(html, 'lxml')
        headers = self.soup.find_all(attrs={"class": "LC20lb"})
        self.links = []
        for h in headers:
            self.links.append(h.parent.attrs['href'])
        self.headers = [x.text for x in headers]
        abstracts = self.soup.find_all('span', {'class' : 'st'})
        self.abstracts = [x.text for x in abstracts]
        self.results = list(zip(self.headers, self.links, self.abstracts))
        
        self.top_domains = [urlparse(x).netloc.split(".")[-2] + "." + urlparse(x).netloc.split(".")[-1] for x in self.links]


    def get_result_soup(self, index=0):
        html = requests.get(self.links[index]).content
        soup = BeautifulSoup(html, 'lxml')
        return(soup)


    def get_potential_text(self, index=0):
        soup = self.get_result_soup(index=index)
        ps = soup.find_all("p")
        return("\n".join([str(x) for x in ps]).replace("<p>", "\n").replace("</p>", "").replace("\n\n", "\n")[1:])
    
    
class GoogleResultFiles():
    
    def __init__(self, paths=[]):
        directories = [x for x in paths if Path(x).is_dir()]
        self.files = [x for x in paths if Path(x).is_file() and Path(x).suffix == ".html"]
        self.files = [x for x in paths if Path(x).is_file() and Path(x).suffix == ".htm"]
        for x in directories: self.files.extend(list(Path(x).glob("*.html")))
        for x in directories: self.files.extend(list(Path(x).glob("*.htm")))
        
        self.result_files = [GoogleResultFile(x) for x in self.files]
        self.results = []
        for x in self.result_files: self.results.extend(x.results)
        
        self.headers = []
        for x in self.result_files: self.headers.extend(x.headers)
        
        self.links = []
        for x in self.result_files: self.links.extend(x.links)
        
        self.abstracts = []
        for x in self.result_files: self.abstracts.extend(x.abstracts)
        
        self.top_domains = []
        for x in self.result_files: self.top_domains.extend(x.top_domains)